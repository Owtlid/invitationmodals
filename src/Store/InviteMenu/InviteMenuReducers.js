import * as type from "./InviteMenuTypes";

const initialState = {
  modalStatus: {
    linkModalIsOpen: false,
    codeModalIsOpen: false,
  },
};

const InviteMenuReducers = (state = initialState, action) => {
  switch (action.type) {
    case type.INTERACT_WITH_LINK_MODAL:
      return {
        ...state,
        modalStatus: { linkModalIsOpen: !state.modalStatus.linkModalIsOpen },
      };
    case type.INTERACT_WITH_CODE_MODAL:
      return {
        ...state,
        modalStatus: { codeModalIsOpen: !state.modalStatus.codeModalIsOpen },
      };
    default:
      return state;
  }
};

export default InviteMenuReducers;
