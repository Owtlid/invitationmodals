import * as type from "./InviteMenuTypes";

const linkModalAction = () => {
  return {
    type: type.INTERACT_WITH_LINK_MODAL,
  };
};

const codeModalAction = () => {
  return {
    type: type.INTERACT_WITH_CODE_MODAL,
  };
};

export { codeModalAction, linkModalAction };
