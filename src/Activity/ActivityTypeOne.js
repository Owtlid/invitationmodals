import { Box, Button, Dialog, Typography } from "@material-ui/core";
import useStyle from "./ActivityTypeOne.style";
import src1 from "../Assets/ActivitiesEdit.svg";
import src2 from "../Assets/ActivitiesClose.svg";
import calendaIcon from "../Assets/calendarIcon.svg";
import labelIcon from "../Assets/LabelIcon.svg";

const ActivityTypeOne = ({ status, setStatus }) => {
  const classes = useStyle();
  const handelClose = () => {
    setStatus((prevVal) => !prevVal);
  };
  return (
    <Dialog
      open={status}
      fullWidth
      maxWidth="sm"
      onClose={handelClose}
      PaperProps={{
        style: {
          borderRadius: "12px",
          width: "400px",
          height: "620px",
        },
      }}
    >
      <Box className={classes.header}>
        <Typography className={classes.title}>Webiz Academy Webinar</Typography>
          <img
          className={classes.edit_icon}
            src={src1}
            width="24px"
            height="24px"
            alt=""
          />
          <img
          className={classes.close_icon}
            src={src2}
            width="24px"
            height="24px"
            alt=""
          />
      </Box>
      <Typography className={classes.main_text}>
        Audio player software is used to play back sound recordings in one of
        the many formats available for computers today. It can also play back
        music CDs. There is audio player software that is native to the
        computer’s operating system (Windows, Macintosh)
      </Typography>
      <Box className={classes.border}></Box>
      <Box className={classes.calendar}>
        <span className={classes.calendar_icon}>
          <img
            src={calendaIcon}
            width="24px"
            height="24px"
            alt=""
          />
        </span>
        <Typography className={classes.calendar_text}>
          Monday, January 11
        </Typography>
      </Box>
      <Typography className={classes.plain_text}>
        Tuesday, January 12
      </Typography>
      <Typography className={classes.plain_text}>
        Wednesday, January 13
      </Typography>
      <Typography className={classes.plain_text}>
        Tuesday, January 12
      </Typography>
      <Typography className={classes.plain_text}>
        Wednesday, January 13
      </Typography>
      <Button className={classes.show_all_BTN}> Show all dates</Button>
      <Box className={classes.label}>
      <span className={classes.label_icon}>
        <img src={labelIcon} alt='' width='24px' height='24px' />
        </span>
        <Typography className={classes.label_text}>Free</Typography>
      </Box>
      <Button className={classes.massive_btn}>Select Activity</Button>
    </Dialog>
  );
};

export default ActivityTypeOne;
