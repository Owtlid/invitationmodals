import React from "react";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import CodeIcon from "@material-ui/icons/Code";
import InvitationModals from "./InvitationModals";
import useStyles from "./InviteMenu.styles";
import { useDispatch } from "react-redux";
import {
  codeModalAction,
  linkModalAction,
} from "../Store/InviteMenu/InviteMenuActions";

const InviteMenu = () => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);
  const dispatch = useDispatch();

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (event.target.innerText === "Invite with Link") {
      dispatch(linkModalAction());
    }
    if (event.target.innerText === "Share Community code") {
      dispatch(codeModalAction());
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    }
  }

  return (
    <div className={classes.root}>
      <div>
        <Button
          ref={anchorRef}
          aria-controls={open ? "menu-list-grow" : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
          className={classes.inviteButton}
        >
          Toggle Menu Grow
        </Button>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          <Paper className={classes.inviteTypes}>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList
                autoFocusItem={open}
                id="menu-list-grow"
                onKeyDown={handleListKeyDown}
              >
                <MenuItem
                  className={classes.inviteTypeItems}
                  onClick={handleClose}
                >
                  <MailOutlineIcon fontSize="small" /> Email Invitation
                </MenuItem>
                <MenuItem
                  className={classes.inviteTypeItems}
                  onClick={handleClose}
                >
                  <AttachFileIcon fontSize="small" /> Invite with Link
                </MenuItem>
                <MenuItem
                  className={classes.inviteTypeItems}
                  onClick={handleClose}
                  style={{ borderBottom: "none", paddingBottom: "0px" }}
                >
                  <CodeIcon fontSize="small" /> Share Community code
                </MenuItem>
              </MenuList>
            </ClickAwayListener>
          </Paper>
          )}
        </Popper>
      </div>
      <InvitationModals />
    </div>
  );
};

export default InviteMenu;
