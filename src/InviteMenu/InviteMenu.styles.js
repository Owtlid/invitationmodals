import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  inviteButton: {
    borderRadius: "8px",
    backgroundColor: "#5EBE84",
    width: "209px",
    height: "40px",
    fontSize: "12px",
    fontWeight: "500",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#5EBE84",
    },
  },
  inviteTypes: {
    width: "209px",
    borderRadius: "5px",
    marginTop: "15px",
  },
  inviteTypeItems: {
    fontSize: "12px",
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: "20px",
    marginTop: "8px",
    marginBottom: "8px",
    borderBottom: "1px solid #B8B8B8",
  },
}));

export default useStyles;
