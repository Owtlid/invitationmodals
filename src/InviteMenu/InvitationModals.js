import React, { useEffect, useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import InputAdornment from "@material-ui/core/InputAdornment";
import CloseIcon from "@material-ui/icons/Close";
import { Box, Button, TextField } from "@material-ui/core";
import imgLink from "../Assets/connection.svg";
import scndImgLink from "../Assets/connection2.svg";
import AttachFileIcon from "@material-ui/icons/AttachFile";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import CodeIcon from "@material-ui/icons/Code";
import styles from "./InvitationModals.styles";
import { useDispatch, useSelector } from "react-redux";
import {
  codeModalAction,
  linkModalAction,
} from "../Store/InviteMenu/InviteMenuActions";

const InvitationModals = ({ link, openModal, setOpenModal }) => {
  const classes = styles();
  const [inputText, setInputText] = useState("");
  const [copied, setCopied] = useState(false);
  const status = useSelector((state) => state.modalStatus);
  const dispatch = useDispatch();

  const handleClose = () => {
    if (status.linkModalIsOpen) {
      dispatch(linkModalAction());
    }
    if (status.codeModalIsOpen) {
      dispatch(codeModalAction());
    }
  };

  useEffect(() => {
    status.linkModalIsOpen && setInputText("link");
    status.codeModalIsOpen && setInputText("code");
  }, [status]);

  useEffect(() => {
    if (copied) {
      const timer = setInterval(() => {
        setCopied((prevVal) => !prevVal);
      }, 1000);
      return () => {
        clearInterval(timer);
      };
    }
  }, [copied]);

  const copyFunc = () => {
    navigator.clipboard.writeText(inputText);
    setCopied((prevVal) => !prevVal);
  };

  return (
    <div>
      <Dialog
        className={classes.invitationModal}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={status.linkModalIsOpen || status.codeModalIsOpen}
        fullWidth
        maxWidth="sm"
      >
        <Box className={classes.invitationCloseButton}>
          <CloseIcon onClick={handleClose} />
        </Box>
        <Box className={classes.invitationHeader}>
          {status.linkModalIsOpen && "Invitation Link"}
          {status.codeModalIsOpen && "Share Community code"}
        </Box>
        <Box className={classes.invitationText}>
          <span>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            lobortis ante non cursus fermentum. Donec mattis sed metus vel
            cursus. Nulla facilisi.Pellentesque quis urna non libero tincidunt
            tincidunt vel commodo purus.
          </span>
        </Box>
        <Box className={classes.modalImg}>
          {status.linkModalIsOpen && (
            <img src={imgLink} alt="" width={"350px"} height={"300px"} />
          )}
          {status.codeModalIsOpen && (
            <img src={scndImgLink} alt="" width={"350px"} height={"300px"} />
          )}
        </Box>
        <Box className={classes.invitationInput}>
          <TextField
            variant="outlined"
            value={inputText}
            onChange={(e) => {
              setInputText(e.target.value);
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {status.linkModalIsOpen && (
                    <AttachFileIcon fontSize="small" />
                  )}
                  {status.codeModalIsOpen && <CodeIcon fontSize="small" />}
                </InputAdornment>
              ),
            }}
          />
          <Button className={classes.invitationInputCopy} onClick={copyFunc}>
            <FileCopyIcon fontSize="small" />
            {copied ? "Copied" : "Copy"}
          </Button>
        </Box>
      </Dialog>
    </div>
  );
};

export default InvitationModals;
