import { makeStyles } from "@material-ui/core/styles";

const styles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  invitationCloseButton: {
    width: "90%",
    padding: "20px",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    cursor: "pointer",
  },
  invitationHeader: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: "22px",
    fontWeight: "500",
    marginBottom: "20px",
  },
  invitationText: {
    width: "70%",
    margin: "0 auto",
    fontSize: "14px",
    fontWeight: "400",
    textAlign: "center",
    lineHeight: "2",
  },
  modalImg: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    margin: "10px 0",
  },
  invitationInput: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "50px 0",
    border: "none",
  },
  invitationInputCopy: {
    backgroundColor: "#DFF2E6",
    height: "55px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: "0 10px",
    borderRadius: "8px",
    color: "rgb(174,222,192)",
    marginLeft: "10px",
  },
}));

export default styles;
