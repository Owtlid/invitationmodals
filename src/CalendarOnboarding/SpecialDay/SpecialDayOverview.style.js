import { makeStyles } from "@material-ui/core";
import img from "../../Assets/SpecialDayOverviewHeader.svg";

const useStyleSpecial = makeStyles((theme) => ({
  header: {
    height: "80px",
    backgroundColor: "#5EBE84",
    backgroundImage: `url(${img})`,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    marginLeft: "36px",
    marginTop: "28px",
    marginBottom: "28px",
    fontSize: "18px",
    fontWeight: "600",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.33",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#fff",
  },
  closeBtn: {
    marginLeft: "128px",
    cursor: "pointer",
    "&:active": {
      opacity: 0.5,
    },
  },
  content: {
    padding: '22px 32px 40px 32px',
  },
  firstLine: {
    fontSize: "14px",
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    letterSpacing: 'normal',
    lineHeight: "1.71",
    textAlign: "left",
    color: "rgba(29, 52, 82, 0.5)",
    marginBottom: '16px',
  },
  secondLine: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '16px',
  },
  secondLine__text: {
    fontSize: "14px",
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    letterSpacing: 'normal',
    lineHeight: "1.71",
    textAlign: "left",
    color: "rgba(29, 52, 82, 0.5)",
  },
  android_arrowLeft: {
    width: "16px",
    height: "16px",
    margin: "4px 4px 4px 12px",
    objectFit: "contain",
  },
  android_arrowRight: {
    width: "16px",
    height: "16px",
    margin: "4px 12px 4px 4px",
    objectFit: "contain",
  },
  thirdLine: {
    fontSize: "14px",
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    letterSpacing: 'normal',
    lineHeight: "1.71",
    textAlign: "left",
    color: "rgba(29, 52, 82, 0.5)",
  },
}));

export default useStyleSpecial;
