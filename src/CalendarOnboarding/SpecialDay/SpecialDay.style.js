import { makeStyles } from "@material-ui/core";
import img from "../../Assets/SpecialDayHeader.svg";

const useStyle = makeStyles((theme) => ({
  header: {
    height: "80px",
    backgroundColor: "#5EBE84",
    backgroundImage: `url(${img})`,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    height: "24px",
    marginLeft: "36px",
    marginTop: "28px",
    marginBottom: "28px",
    fontSize: "17px",
    fontWeight: "600",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.33",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#fff",
  },
  closeBtn: {
    marginLeft: "128px",
    cursor: "pointer",
    "&:active": {
      opacity: 0.5,
    },
  },
  content: {
    padding: '22px 32px 35px 32px',
  },
  firstLine: {
    fontSize: "14px",
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    letterSpacing: 'normal',
    lineHeight: "1.71",
    textAlign: "left",
    color: "rgba(29, 52, 82, 0.5)",
    marginBottom: '10px',
  },
  secondLine: {
    display: 'flex',
    alignItems: 'center',
  },
  secondLine__text: {
    paddingTop: '3px',
    paddingBottom: '5px',
    fontSize: "14px",
    fontWeight: 'normal',
    fontStretch: 'normal',
    fontStyle: 'normal',
    letterSpacing: 'normal',
    lineHeight: "1.71",
    textAlign: "left",
    color: "rgba(29, 52, 82, 0.5)",
  },
  tapIcon: {
    width: "32px",
    height: "32px",
    margin: '0 10px 0 8px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EBEFF5',
    cursor: 'pointer',
  },
  stepper: {
    padding: '0 36px 0 32px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dots: {
    width: "48px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
  singleDot: {
    width: "8px",
    height: "8px",
    boxShadow: "0 2px 8px 0 rgba(0, 0, 0, 0.12)",
    backgroundColor: "#8f9aa9",
    borderRadius: "50%",
  },

  RroundButton: {
    border: "solid 8px #dff2e6",
    backgroundColor: "#5ebe84",
    borderRadius: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
    padding: "6px",
    minWidth: '20px',
    boxShadow: 'none',
    "&:hover": {
      backgroundColor: "#5ebe84",
      boxShadow: 'none',
    },
    "&:active": {
      opacity: 0.5,
    },
  },
  LroundButton: {
    borderRadius: "50%",
    padding: "6px",
    backgroundColor: "#EAF7EF",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
    boxShadow: 'none',
    minWidth: '20px',
    "&:hover": {
      boxShadow: 'none'
    },
    "&:active": {
      opacity: 0.5,
    },
  },
}));

export default useStyle;
