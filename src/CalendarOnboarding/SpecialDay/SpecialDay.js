import { Box, Dialog, Button, Typography } from "@material-ui/core";
import useStyle from "./SpecialDay.style";
import closeIcon from "../../Assets/Close.svg";
import tapIcon from "../../Assets/Tap.svg";
import RArrow from "../../Assets/Arrow.svg";
import LArrow from "../../Assets/LArrow.svg";
import useStyleSpecial from "./SpecialDayOverview.style";
import { useState } from "react";
import r_android_arrow from "../../Assets/R_Android_Arrow.svg";
import l_android_arrow from "../../Assets/L_Android_Arrow.svg";

const dialogSteps = [
  {
    step: 0,
  },
  {
    step: 1,
  },
];

const SpecialDay = ({ status, setOpn }) => {
  const classes = useStyle();
  const classesSpecial = useStyleSpecial();

  const [activeStep, setActiveStep] = useState(0);
  const maxSteps = dialogSteps.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    if (activeStep === maxSteps - 1) {
      setOpn(pravVAl => !pravVAl);
    }
  };

  const handlePrev = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    if (activeStep - 1 < 0) {
      setOpn(pravVAl => !pravVAl);
    }
  };

  const handelClse = () => {
    setOpn(prevVal => !prevVal);
  };

  return (
    <Dialog
      open={status}
      fullWidth
      maxWidth="sm"
      onClose={handelClse}
      PaperProps={{
        style: {
          borderRadius: "12px",
          width: "480px",
          height:
            dialogSteps &&
            dialogSteps[activeStep] &&
            dialogSteps[activeStep].step === 0
              ? "294px"
              : "358px",
        },
      }}
    >
      <Box>
        {dialogSteps &&
          dialogSteps[activeStep] &&
          dialogSteps[activeStep].step === 0 && (
            <>
              <Box className={classes.header}>
                <Typography className={classes.title}>
                  Let’s Open your first special day
                </Typography>
                <img
                  src={closeIcon}
                  className={classes.closeBtn}
                  width="24px"
                  height="24px"
                  alt=""
                  onClick={handelClse}
                />
              </Box>
              <Box className={classes.content}>
                <Typography className={classes.firstLine}>
                  Opening any special day, you will be able to see all the
                  amazing related activities, contents & packages offered by WIV
                  team.
                </Typography>
                <Box className={classes.secondLine}>
                  <Typography className={classes.secondLine__text}>To view offerings please tap</Typography>
                  
                  <span className={classes.tapIcon}>
                    <img src={tapIcon} alt="" width='20px' height='20px'/>
                  </span>
                  <Typography className={classes.secondLine__text}>to open the modal</Typography>
                </Box>
              </Box>
            </>
          )}

        {dialogSteps &&
          dialogSteps[activeStep] &&
          dialogSteps[activeStep].step === 1 && (
            <>
              <Box className={classes.header}>
                <Typography className={classesSpecial.title}>
                  Let’s see special day overview
                </Typography>
                <img
                  src={closeIcon}
                  className={classesSpecial.closeBtn}
                  width="24px"
                  height="24px"
                  alt=""
                  onClick={handelClse}
                />
              </Box>
              <Box className={classesSpecial.content}>
                <Typography className={classesSpecial.firstLine}>
                  On special day overview you can see all the offerings for
                  <Typography style={{ color: "rgb(96,113,134)",  fontSize: "14px" }}>
                    Activities, Content, Packages.
                  </Typography>
                </Typography>
                <Box className={classesSpecial.secondLine} >
                  <Typography className={classesSpecial.secondLine__text}>Use arrows</Typography>
                  
                  <img
                    className={classesSpecial.android_arrowLeft}
                    src={l_android_arrow}
                    alt=""
                  />
                  <img
                    className={classesSpecial.android_arrowRight}
                    src={r_android_arrow}
                    alt=""
                  />
                  <Typography className={classesSpecial.secondLine__text}>to move through suggestions</Typography>
                  
                </Box>
                <Typography className={classesSpecial.thirdLine}>
                  If there is no offer to your liking you can always {' '}
                  <Typography
                    style={{ textDecoration: "underline", color: "#6B7B8F", display: 'inline' , fontSize: "14px"}}
                  >
                    create from scratch
                  </Typography>
                </Typography>
              </Box>
            </>
          )}

        <Box
          className={classes.stepper}
        >
          <Button
            className={classes.LroundButton}
            variant="contained"
            onClick={handlePrev}
          >
            <img src={LArrow} width="20px" height="20px" alt="" />
          </Button>
          <Box className={classes.dots}>
            <Box
              className={classes.singleDot}
              style={{
                backgroundColor: [activeStep === 0 ? "#ff9432" : "#8f9aa9"],
              }}
            ></Box>
            <Box
              className={classes.singleDot}
              style={{
                backgroundColor: [activeStep === 1 ? "#ff9432" : "#8f9aa9"],
              }}
            ></Box>
          </Box>
          <Button
            className={classes.RroundButton}
            variant="contained"
            onClick={handleNext}
          >
            <img src={RArrow} width="20px" height="20px" alt="" />
          </Button>
        </Box>
      </Box>
    </Dialog>
  );
};

export default SpecialDay;
