import { makeStyles } from "@material-ui/core";

const useStyleThree = makeStyles((theme) => ({
  header: {
    width: "600px",
    height: "240px",
  },
  title: {
    padding: "0 5px 16px",
    fontSize: "20px",
    fontWeight: "800",
    fontStretch: 'normal',
    fontStyle: 'normal',
    letterSpacing: 'normal',
    textAlign: "center",
    color: "#1d3452",
  },
  text: {
    padding: '16px 0 0',
    fontSize: "16px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.75",
    letterSpacing: "normal",
    textAlign: "center",
    color: "#8f9aa9",
  },
  content: {
    marginTop: '52px',
    padding: '0 155px 0 155px',
    marginBottom: '75px'
  },
}));

export default useStyleThree;
