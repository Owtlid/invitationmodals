import { makeStyles } from "@material-ui/core";
import bckIMG from "../../Assets/StepTwoHeader.png";

const useStyleTwo = makeStyles((theme) => ({
  dialog: {
    width: "600px",
    height: "774px",
  },
  header: {
    width: "600px",
    height: "240px",
    backgroundImage: `url(${bckIMG})`,
    backgroundPosition: "center",
    backgroundSize: "100%",
    marginBottom: '60px',
  },
  title: {
    padding: '71px 160px 0 160px',
    fontSize: "20px",
    fontWeight: "800",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "0.8",
    letterSpacing: "normal",
    textAlign: "center",
    color: "#fff",
    marginBottom: '16px',
  },
  text: {
    padding: '0 120px 73px 120px',
    fontSize: "16px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.75",
    letterSpacing: "normal",
    textAlign: "center",
    color: "#fff",
  },
  content__item: {
    padding: '0 72px 0 72px',
    marginBottom: "42px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  item__img: {
    width: "72px",
    height: "72px",
    padding: "20px",
    borderRadius: "8px",
    backgroundColor: "#f4efff",
    boxSizing: "border-box",
  },
  item__desc: {
    marginLeft: '24px',
  },
  desc_title: {
    fontSize: "16px",
    fontWeight: "600",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.25",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#1d3452",
    marginBottom: '8px',
  },
  desc_text: {
    padding: "8px 0 0",
    fontSize: "13px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.69",
    letterSpacing: "normal",
    textAlign: "left",
    color: "rgba(29, 52, 82, 0.5)",
  },
}));

export default useStyleTwo;
