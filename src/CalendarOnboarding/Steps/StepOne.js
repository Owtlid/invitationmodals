import { Box, Button, Dialog, Typography } from "@material-ui/core";
import { useState } from "react";
import arrow from "../../Assets/Arrow.svg";
import StepOneImg from "../../Assets/StepOneHeader.png";
import useStyleOne from "./StepOne.style";
import useStyleTwo from "./StepTwo.style";
import useStyleThree from "./StepThree.style";
import activities from "../../Assets/Activities.svg";
import create from "../../Assets/Create.svg";
import purchase from "../../Assets/Purchase.svg";
import StepThreeImg from "../../Assets/StepThreeHeader.png";

const dialogSteps = [
  {
    step: 0,
  },
  {
    step: 1,
  },
  {
    step: 2,
  },
];

const StepOne = ({ status ,setOpn }) => {
  const [activeStep, setActiveStep] = useState(0);
  const maxSteps = dialogSteps.length;
  const classesOne = useStyleOne();
  const classesTwo = useStyleTwo();
  const classesThree = useStyleThree();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    if (activeStep === maxSteps - 1) {
      setOpn(prevVal => !prevVal);
    }
  };

  const handelClse = () => {
    setOpn(prevVal => !prevVal);
  };

  return (
    <Dialog
      open={!status}
      fullWidth
      maxWidth="sm"
      onClose={handelClse}
      PaperProps={{
        style: { borderRadius: "12px", maxWidth: '600px'},
      }}
    >
      <Box>
        {dialogSteps &&
          dialogSteps[activeStep] &&
          dialogSteps[activeStep].step === 0 && (
            <>
              <Box className={classesOne.header}>
                <img width="100%" height="100%" src={StepOneImg} alt="" />
              </Box>
              <Box className={classesOne.content}>
                  <Typography className={classesOne.text__title}>
                    Welcome on WIV calendar
                  </Typography>
                  <Typography className={classesOne.text__text}>
                    This is the page where you can manage your events and
                    milestones
                  </Typography>
              </Box>
            </>
          )}
        {dialogSteps &&
          dialogSteps[activeStep] &&
          dialogSteps[activeStep].step === 1 && (
            <>
              <Box className={classesTwo.header}>
                <Typography className={classesTwo.title}>
                  What is Milestone?
                </Typography>
                <Typography className={classesTwo.text}>
                  Milestone is event day which is combination of these three
                  different component:
                </Typography>
              </Box>
                <Box className={classesTwo.content__item}>
                  <Box className={classesTwo.item__img}>
                    <img src={activities} alt="" width="32px" height="32px" />
                  </Box>
                  <Box className={classesTwo.item__desc}>
                    <Typography className={classesTwo.desc_title}>
                      1. Plan your activities
                    </Typography>
                    <Typography className={classesTwo.desc_text}>
                      Amet minim mollit non deserunt ullamco est sit aliqua
                      dolor do amet sint. Velit officia consequat duis enim
                      velit.{" "}
                    </Typography>
                  </Box>
                </Box>

                <Box className={classesTwo.content__item}>
                  <Box className={classesTwo.item__img}>
                    <img src={create} alt="" width="32px" height="32px" />
                  </Box>
                  <Box className={classesTwo.item__desc}>
                    <Typography className={classesTwo.desc_title}>
                      2. Create and manage content
                    </Typography>
                    <Typography className={classesTwo.desc_text}>
                      Amet minim mollit non deserunt ullamco est sit aliqua
                      dolor do amet sint. Velit officia consequat duis enim
                      velit.
                    </Typography>
                  </Box>
                </Box>

                <Box
                  className={classesTwo.content__item}
                  style={{ marginBottom: "91px" }}
                >
                  <Box className={classesTwo.item__img}>
                    <img src={purchase} alt="" width="32px" height="32px" />
                  </Box>
                  <Box className={classesTwo.item__desc}>
                    <Typography className={classesTwo.desc_title}>
                      3. Purchase packages
                    </Typography>
                    <Typography className={classesTwo.desc_text}>
                      Amet minim mollit non deserunt ullamco est sit aliqua
                      dolor do amet sint. Velit officia consequat duis enim
                      velit.
                    </Typography>
                  </Box>
                </Box>
            </>
          )}

        {dialogSteps &&
          dialogSteps[activeStep] &&
          dialogSteps[activeStep].step === 2 && (
            <>
              <Box className={classesThree.header}>
                <img width="100%" height="100%" src={StepThreeImg} alt="" />
              </Box>
              <Box className={classesThree.content}>
                  <Typography className={classesThree.title}>
                  What is Special day?
                  </Typography>
                  <Typography className={classesThree.text}>
                  This is the page where you can manage your events and milestones
                  </Typography>
              </Box>
            </>
          )}

        <Box className={classesOne.line}>
          <Button
            className={classesOne.roundButton}
            variant="contained"
            onClick={handleNext}
          >
            <img src={arrow} width="24px" height="24px" alt="" />
          </Button>
        </Box>

        <Box className={classesOne.footer}>
          <Box className={classesOne.dots}>
            <Box
              className={classesOne.singleDot}
              style={{
                backgroundColor: [activeStep === 0 ? "#ff9432" : "#8f9aa9"],
              }}
            ></Box>
            <Box
              className={classesOne.singleDot}
              style={{
                backgroundColor: [activeStep === 1 ? "#ff9432" : "#8f9aa9"],
              }}
            ></Box>
            <Box
              className={classesOne.singleDot}
              style={{
                backgroundColor: [activeStep === 2 ? "#ff9432" : "#8f9aa9"],
              }}
            ></Box>
          </Box>
          <Box className={classesOne.skip} onClick={handelClse}>
            Skip intro
          </Box>
        </Box>
      </Box>
    </Dialog>
  );
};

export default StepOne;
