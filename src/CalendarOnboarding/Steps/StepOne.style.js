import { makeStyles } from "@material-ui/core";

const useStyleOne = makeStyles((them) => ({
  dialog: {
    width: "600px",
    height: "548px",
    boxShadow: "0 10px 50px 0 rgba(29, 52, 82, 0.1)",
    backgroundColor: "var(--color-dark-grey-000)",
  },
  header: {
    width: "600px",
    height: "240px",
  },
  content: {
    padding: '52px 155px 75px',
  },
  text__title: {
    fontSize: "20px",
    fontWeight: "800",
    fontStyle: 'normal',
    fontStretch: 'normal',
    letterSpacing: 'normal',
    textAlign: "center",
    color: "#1d3452",
    marginBottom: '16px',
  },
  text__text: {
    fontSize: "16px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.75",
    letterSpacing: "normal",
    textAlign: "center",
    color: "#8f9aa9",
  },
  line: {
    width: "100%",
    height: "1px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: " #f2f5fc",
  },
  roundButton: {
    border: "solid 12px #dff2e6",
    backgroundColor: "#5ebe84",
    borderRadius: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
    padding: "12px",
    boxShadow: 'none',
    "&:hover": {
      backgroundColor: "#5ebe84",
      boxShadow: 'none',
    },
    "&:active": {
      opacity: 0.5,
    },
  },
  footer: {
    width: "496px",
    margin: "0 auto",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: "24px",
    marginBottom: '32px',
  },
  dots: {
    width: "48px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
  skip: {
    fontSize: "16px",
    fontWeight: "normal",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "1.75",
    letterSpacing: "normal",
    textAlign: "left",
    color: "#8f9aa9",
    cursor: "pointer",
    "&:active": {
      opacity: 0.5,
    },
  },
  singleDot: {
    width: "8px",
    height: "8px",
    boxShadow: "0 2px 8px 0 rgba(0, 0, 0, 0.12)",
    backgroundColor: "#8f9aa9",
    borderRadius: "50%",
  },
}));

export default useStyleOne;
