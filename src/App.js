import { useState } from "react";
import "./App.css";
// import InviteMenu from "./InviteMenu/InviteMenu";
import StepOne from "./CalendarOnboarding/Steps/StepOne";
import SpecialDay from "./CalendarOnboarding/SpecialDay/SpecialDay";
import { Button } from "@material-ui/core";
import ActivityTypeOne from "./Activity/ActivityTypeOne";

function App() {

  const [open, setOpen] = useState(false);
  
  const [activityTypeOne, setActivityTypeOne] = useState(false);
  const [activityTypeTwo, setActivityTypeTwo] = useState(false);
  
  return (
    <div className="App">
      {/* <Button variant='contained' onClick = {() => setOpen(prevVal => !prevVal)}>Open Calendar</Button> */}
      {/* <InviteMenu /> */}
      {/* <StepOne status={open} setOpn = {setOpen}/> */}
      {/* <SpecialDay status={open} setOpn={setOpen}/> */}

      <Button variant='contained' onClick = {() => setActivityTypeOne(prevVal => !prevVal)}>Open Activity One</Button>
      <Button variant='contained' onClick = {() => setActivityTypeTwo(prevVal => !prevVal)}>Open Activity Two</Button>

      <ActivityTypeOne status={activityTypeOne} setStatus={setActivityTypeOne} />

    </div>
  );
}

export default App;
